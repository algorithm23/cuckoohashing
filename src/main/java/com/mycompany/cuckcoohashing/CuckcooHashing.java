/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.cuckcoohashing;

/**
 *
 * @author informatics
 */
public class CuckcooHashing {

    static int key = 11;
    static int value = 2;
    static int[][] hashtable = new int[value][key];
    static int[] arr = new int[value];

    static void Table() {
        for (int j = 0; j < key; j++) {
            for (int i = 0; i < value; i++) {
                hashtable[i][j] = Integer.MIN_VALUE;
            }
        }
    }

    static int hash(int function, int key) {
        switch (function) {
            case 1:
                return key % CuckcooHashing.key;
            case 2:
                return (key / CuckcooHashing.key) % CuckcooHashing.key;
        }
        return Integer.MIN_VALUE;
    }

    static void get(int key, int table, int ck, int n) {
        for (int i = 0; i < value; i++) {
            arr[i] = hash(i + 1, key);
            if (hashtable[i][arr[i]] == key) {
                return;
            }
        }

        if (hashtable[table][arr[table]] != Integer.MIN_VALUE) {
            int dis = hashtable[table][arr[table]];
            hashtable[table][arr[table]] = key;
            get(dis, (table + 1) % value, ck + 1, n);
        } else {
            hashtable[table][arr[table]] = key;
        }
    }

    static void printTable() {

        System.out.printf("Hash Table : \n");

        for (int i = 0; i < value; i++, System.out.printf("\n")) {
            for (int j = 0; j < key; j++) {
                if (hashtable[i][j] == Integer.MIN_VALUE) {
                    System.out.printf("- ");
                } else {
                    System.out.printf("%d ", hashtable[i][j]);
                }
            }
        }
        System.out.printf("\n");
    }
    
    static void cuckoo(int keys[], int n) {
        Table();
        for (int i = 0, cnt = 0; i < n; i++, cnt = 0) {
            get(keys[i], 0, cnt, n);
        }
        printTable();
    }
           
    public static void main(String[] args) {
        
        System.out.println("*********************************************");
        int keys_1[] = {10, 50, 97, 70, 10, 6, 88, 40, 20, 9};
        int n = keys_1.length;
        cuckoo(keys_1, n);

        int keys_2[] = {21, 44, 53, 75, 64, 1, 99, 3, 91, 82, 52, 7};
        int m = keys_2.length;
        cuckoo(keys_2, m);
        System.out.println("*********************************************");
        
    }
    
}
